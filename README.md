Flickr-Demo-Project
=====================

Demo Project for Try Catch


** This project has the following features 

- Simple and clean UI.
- Getting and displaying photos tagged as “Party” from Flickr.
- Getting and displaying user searched photos from Flickr. 
- Getting and displaying Interesting photos from Flickr.
- Getting and displaying photos based on user’s location from Flickr.
- Simple and elegant photo viewer that includes UIGestures.


** This project is created using Cocoapods and uses the following libraries:

- Masonry
- AFNetworking
- SDWebImage
- MBProgressHUD
