//
//  FlickrResultsViewController.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrRequest.h"

@interface FlickrResultsViewController : UIViewController

@property (nonatomic,strong) FlickrRequest *requestObject;

@end
