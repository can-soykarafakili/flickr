//
//  FlickrResultsViewController.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrResultsViewController.h"
#import "FlickrNavigationBarStyler.h"
#import "FlickrImageCell.h"
#import "FlickrNetworkManager.h"
#import "FlickrPhoto.h"
#import "FlickrViewControllerBuilder.h"
#import "UIImageView+WebCache.h"
#import "UIView+Loading.h"
#import <Masonry/Masonry.h>

@interface FlickrResultsViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) NSMutableArray *resultArray;

@property (nonatomic) BOOL isLoading;
@property (nonatomic) NSInteger pageNumber;

@end

@implementation FlickrResultsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [FlickrNavigationBarStyler styleLeftNavigationItemWithBackIcon:self.navigationItem target:self action:@selector(goBack) andWithTitle:self.requestObject.tag ? self.requestObject.tag : self.requestObject.type];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self loadSubviews];
    [self addSubviews];
    [self addLayoutConstraints];
    self.pageNumber = 1;
    [self getPhotosFromFlickr];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)loadSubviews
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    
    [self.collectionView registerClass:[FlickrImageCell class] forCellWithReuseIdentifier:NSStringFromClass([FlickrImageCell class])];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor blackColor];
    
    [self.refreshControl addTarget:self
                            action:@selector(refreshFeed)
                  forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:self.refreshControl];
}

- (void)addSubviews
{
    [self.view addSubview:self.collectionView];
}

- (void)addLayoutConstraints
{
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_topLayoutGuide);
        make.bottom.equalTo(self.mas_bottomLayoutGuide);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
    }];
}

#pragma mark - Collection View Delegate & Data Source

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.resultArray count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FlickrImageCell *imageCell = [collectionView dequeueReusableCellWithReuseIdentifier:[FlickrImageCell reuseIdentifier] forIndexPath:indexPath];
    
    FlickrPhoto *photo = [self.resultArray objectAtIndex:indexPath.row];
    [imageCell.imageView sd_setImageWithURL:photo.photoURL];
    return imageCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [FlickrImageCell sizeForWidth:(CGRectGetWidth(self.collectionView.frame))/2];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FlickrPhoto *photo = [self.resultArray objectAtIndex:indexPath.row];
    UIViewController * vc = [FlickrViewControllerBuilder photoDetailViewControllerWithPhotoObject:photo];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)refreshFeed
{
    self.pageNumber = 1;
    [self getPhotosFromFlickr];
}

- (void)getPhotosFromFlickr
{
    [self.view showLoadingView];
    
    [[FlickrNetworkManager sharedManager]getPhotosWithRequestObject:self.requestObject andWithPage:self.pageNumber successBlock:^(NSMutableArray *photos) {
        
        [self.view dismissLoadingView];
        self.resultArray = photos;
        [self.refreshControl endRefreshing];
        [self.collectionView reloadData];
        
    } failureBlock:^(NSError *error) {
        UIAlertView *warning = [[UIAlertView alloc]initWithTitle:@"Flickr"
                                                         message:@"Oops.. Please check your connection."
                                                        delegate:self
                                               cancelButtonTitle:@"Close"
                                               otherButtonTitles:nil, nil];
        
        [warning show];
    }];
    
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Paging

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    if (bottomEdge >= scrollView.contentSize.height - 5)
    {
        // we are at the end
        if(!self.isLoading)
        {
            [self.view showLoadingView];
            self.isLoading = YES;
            self.pageNumber +=1;
            
            [[FlickrNetworkManager sharedManager]getPhotosWithRequestObject:self.requestObject andWithPage:self.pageNumber successBlock:^(NSMutableArray *photos) {
                if(photos.count > 0)
                {
                    [self.resultArray addObjectsFromArray:photos];
                    [self.collectionView reloadData];
                }
                else
                {
                    self.pageNumber-=1;
                }
                self.isLoading = NO;
                [self.view dismissLoadingView];
            } failureBlock:^(NSError *error) {
                
                UIAlertView *warning = [[UIAlertView alloc]initWithTitle:@"Flickr"
                                                                 message:@"Oops.. Please check your connection."
                                                                delegate:self
                                                       cancelButtonTitle:@"Close"
                                                       otherButtonTitles:nil, nil];
                
                [warning show];
            }];
        }
        
    }
    
}

@end
