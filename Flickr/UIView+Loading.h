//
//  UIView+Loading.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Loading)

//Creates a loading view and adds it to self as a subview. Disables user interaction.
- (void)showLoadingView;

//Dismisses the loading view. Enables user interaction.
- (void)dismissLoadingView;

@end
