//
//  NSString+SearchText.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 14/04/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SearchText)

-(NSString*)formatText;

@end
