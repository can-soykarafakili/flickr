//
//  FlickrNetworkManager.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrNetworkManager.h"
#import "FlickrPhoto.h"

static NSInteger kTimeoutInterval = 60;
static NSInteger kNumberOfPhotos = 15;

static NSString * const kFlickrKey = @"7c2e11bde695337f231d2dbbae98f5d3";
static NSString * const kFlickrSecret = @"d151ccb89dd7dbec";
static NSString * const kBaseURL = @"https://api.flickr.com/services/rest/?method=";
static NSString * const kPhotoSearchAPI = @"flickr.photos.search";
static NSString * const kInterestingPhotoAPI = @"flickr.interestingness.getList";

static NSString * const kFlickrPhotosFlickrJSONFormat = @"format=json";
static NSString * const kFlickrPhotosFlickrApiKeyParameter = @"api_key";
static NSString * const kFlickrPhotosFlickrLatitudeParameter = @"lat=";
static NSString * const kFlickrPhotosFlickrLongitudeParameter = @"lon=";
static NSString * const kFlickrPhotosFlickrTagsParameter = @"tags=";
static NSString * const kFlickrPhotosFlickrNoJSONCallback = @"nojsoncallback=1";
static NSString * const kFlickrPhotosFlickrPerPageParameter = @"per_page=";
static NSString * const kFlickrPhotosFlickrPageParameter = @"page=";

@implementation FlickrNetworkManager

#pragma mark - Singleton

+ (instancetype)sharedManager
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        {
            sharedInstance = [[FlickrNetworkManager alloc] init];
            ((FlickrNetworkManager *)sharedInstance).responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/xml", @"text/html", @"application/json", @"text/json", @"text/javascript", @"text/plain"]];
            [((FlickrNetworkManager *)sharedInstance).requestSerializer setTimeoutInterval:kTimeoutInterval];
        }
    });
    return sharedInstance;
}

#pragma mark - Get Photos

-(void)getPhotosWithRequestObject:(FlickrRequest *)requestObject
                      andWithPage:(NSInteger)page
                     successBlock:(void (^)(NSMutableArray *))successBlock
                     failureBlock:(FailureBlock)failureBlock
{
    NSString *requestString = [NSString stringWithFormat:@"%@%@&%@&%@=%@&%@%ld&%@%ld&%@",
                               kBaseURL,
                               ([requestObject.type isEqualToString: @"Interesting"]) ? kInterestingPhotoAPI : kPhotoSearchAPI,
                               kFlickrPhotosFlickrJSONFormat,
                               kFlickrPhotosFlickrApiKeyParameter,kFlickrKey,
                               kFlickrPhotosFlickrPerPageParameter, (long)kNumberOfPhotos,
                               kFlickrPhotosFlickrPageParameter ,(long)page,
                               kFlickrPhotosFlickrNoJSONCallback];
    
    if(requestObject.tag)
    {
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"&%@%@",kFlickrPhotosFlickrTagsParameter,requestObject.tag]];
    }
    
    if(requestObject.latitude && requestObject.longitude)
    {
        requestString = [requestString stringByAppendingString:[NSString stringWithFormat:@"&%@%@&%@%@",kFlickrPhotosFlickrLongitudeParameter,requestObject.longitude,kFlickrPhotosFlickrLatitudeParameter,requestObject.latitude]];
    }
    
    
    [self GET:requestString parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        successBlock([self parsePhotos:responseObject]);
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        
        failureBlock(error);
        
    }];
    
}

#pragma mark - Parse Photos From Response

-(NSMutableArray *)parsePhotos:(NSDictionary *)response
{
    NSDictionary *results = response;
    NSMutableArray *photoURLsLargeImage = [NSMutableArray array];
    
    NSArray *photos = [[results objectForKey:@"photos"] objectForKey:@"photo"];
    
    for (NSDictionary *photo in photos)
    {
        NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_c.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];;
        
        FlickrPhoto *photo = [FlickrPhoto new];
        photo.photoURL = [NSURL URLWithString:photoURLString];
        [photoURLsLargeImage addObject:photo];
        
    }
    
    return photoURLsLargeImage;
}

@end
