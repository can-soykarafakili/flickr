//
//  FlickrViewControllerBuilder.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FlickrRequest.h"
#import "FlickrPhoto.h"

@interface FlickrViewControllerBuilder : NSObject

+ (UIViewController *)rootViewController;
+ (UIViewController *)mainPageViewController;
+ (UIViewController *)resultsViewControllerWithRequestObject:(FlickrRequest *)requestObject;
+ (UIViewController *)photoDetailViewControllerWithPhotoObject:(FlickrPhoto *)photo;

@end
