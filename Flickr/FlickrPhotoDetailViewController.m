//
//  FlickrPhotoDetailViewController.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrPhotoDetailViewController.h"
#import "FlickrNavigationBarStyler.h"
#import <Masonry/Masonry.h>
#import "ScrollViewCentered.h"
#import "UIView+Centering.h"
#import "UIImageView+WebCache.h"

@interface FlickrPhotoDetailViewController () <UIScrollViewDelegate>

@property (nonatomic,strong) ScrollViewCentered *scrollView;
@property (nonatomic,strong) UIImageView *imageView;

@end

@implementation FlickrPhotoDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    [FlickrNavigationBarStyler styleLeftNavigationItemWithBackIcon:self.navigationItem target:self action:@selector(goBack) andWithTitle:@"Photo Detail"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self loadSubviews];
    [self addSubviews];
}

- (void)loadSubviews
{
    self.imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.clipsToBounds = YES;
    [self.imageView sd_setImageWithURL:self.photo.photoURL];
    self.imageView.userInteractionEnabled = YES;
    
    self.scrollView = [[ScrollViewCentered alloc] initWithFrame:self.view.bounds];
    self.scrollView.contentSize = self.imageView.bounds.size;
    self.scrollView.delegate = self;
    
    [self configureZoomScale];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleNavigationBar:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self.imageView addGestureRecognizer:tapGestureRecognizer];
}

- (void)addSubviews
{
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.imageView];
}

- (void)toggleNavigationBar:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.navigationController setNavigationBarHidden:![self.navigationController isNavigationBarHidden] animated:YES];
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ScrollView Methods

-(void)viewWillLayoutSubviews
{
    [self configureZoomScale];
}

-(void)configureZoomScale
{
    CGFloat xZoomScale = self.scrollView.bounds.size.width/self.imageView.bounds.size.width;
    CGFloat yZoomScale = self.scrollView.bounds.size.height/self.imageView.bounds.size.height;
    CGFloat minZoomScale = MIN(xZoomScale,yZoomScale);
    
    self.scrollView.minimumZoomScale = minZoomScale;
    self.scrollView.maximumZoomScale = 4;
    self.scrollView.zoomScale = minZoomScale;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)doubleTapped:(UITapGestureRecognizer *)tapGesture
{
    if (self.scrollView.zoomScale == 1)
    {
        [self.scrollView zoomToRect:CGRectInset(self.imageView.frame, -1, -1) animated:YES];
    }
    else
    {
        [self.scrollView setZoomScale:1 animated:YES];
    }
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self.imageView centerViewInScrollView:scrollView];
    
}

@end
