//
//  FlickrNavigationBarStyler.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FlickrNavigationBarStyler : NSObject

+ (void)style;
+ (void)styleNavigationBarTitle;
+ (void)styleLeftNavigationItemWithBackIcon:(UINavigationItem *)navigationItem
                                     target:(id)target
                                     action:(SEL)action
                               andWithTitle:(NSString *)title;

+ (void)styleNavigationItem:(UINavigationItem *)navigationItem
                  WithTitle:(NSString *)title;

@end
