//
//  NSString+SearchText.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 14/04/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "NSString+SearchText.h"

@implementation NSString (SearchText)

-(NSString *)formatText
{
    
    NSString * searchEditedText = self;
    
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"ü" withString:@"u"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"ç" withString:@"c"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"Ü" withString:@"U"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"Ç" withString:@"C"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"ö" withString:@"o"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"Ö" withString:@"O"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"ğ" withString:@"g"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"Ğ" withString:@"G"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"ş" withString:@"s"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"Ş" withString:@"S"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"ı" withString:@"i"];
    searchEditedText = [searchEditedText stringByReplacingOccurrencesOfString:@"İ" withString:@"I"];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z0-9_]+" options:0 error:nil];
    
    searchEditedText = [regex stringByReplacingMatchesInString:searchEditedText options:0 range:NSMakeRange(0, searchEditedText.length) withTemplate:@""];
    
    return searchEditedText;
    
}

@end
