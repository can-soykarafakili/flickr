//
//  UIView+Centering.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "UIView+Centering.h"

@implementation UIView (Centering)

-(void)centerViewInScrollView:(UIScrollView *)scrollView
{
    //Center the image as it becomes smaller than the size of the screen
    
    //We are getting the bounds size for the UIScrollView which will tell us the height/width of UIScrollView
    CGSize boundsSize = scrollView.bounds.size;
    
    CGRect frameToCenter = self.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
    {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else
    {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
    {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else
    {
        frameToCenter.origin.y = 0;
    }
    
    self.frame = frameToCenter;
}

@end
