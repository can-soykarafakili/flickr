//
//  FlickrNetworkManager.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "FlickrRequest.h"

typedef void (^FailureBlock)(NSError *error);

@interface FlickrNetworkManager : AFHTTPRequestOperationManager

+ (instancetype)sharedManager;

- (void)getPhotosWithRequestObject:(FlickrRequest *)requestObject
                       andWithPage:(NSInteger)page
                      successBlock:(void(^)(NSMutableArray *photos))successBlock
                      failureBlock:(FailureBlock)failureBlock;

@end
