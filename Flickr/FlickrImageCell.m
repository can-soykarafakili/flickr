//
//  FlickrImageCell.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrImageCell.h"
#import <Masonry/Masonry.h>

@implementation FlickrImageCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self loadSubviews];
        [self addSubviews];
        [self addLayoutConstraints];
    }
    
    return self;
}

- (void)loadSubviews
{
    self.imageView = [[UIImageView alloc]init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
}

- (void)addSubviews
{
    [self.contentView addSubview:self.imageView];
}

- (void)addLayoutConstraints
{
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
    }];
}

+ (NSString *)reuseIdentifier
{
    return NSStringFromClass([self class]);
}

+ (CGSize)sizeForWidth:(CGFloat)width
{
    CGSize size = CGSizeMake(width, width * [self cellRatio]);
    return size;
}

+ (CGFloat)cellRatio
{
    return 160.f/160.f;
}

@end
