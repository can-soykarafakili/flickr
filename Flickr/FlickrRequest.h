//
//  FlickrRequest.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrRequest : NSObject

@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *tag;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *latitude;

@end
