//
//  FlickrNavigationBarStyler.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrNavigationBarStyler.h"

@implementation FlickrNavigationBarStyler

+ (void)style
{
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    [FlickrNavigationBarStyler styleNavigationBarTitle];
    
}

+ (void)styleNavigationBarTitle
{
    NSDictionary *settings = @{
                               NSFontAttributeName                 :  [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0],
                               NSForegroundColorAttributeName      :  [UIColor whiteColor]};
    
    [[UINavigationBar appearance] setTitleTextAttributes:settings];
    
    
}

+ (void)styleLeftNavigationItemWithBackIcon:(UINavigationItem *)navigationItem
                                     target:(id)target
                                     action:(SEL)action
                               andWithTitle:(NSString *)title
{
    navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icnBack"] style:UIBarButtonItemStylePlain target:target action:action];
    navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    navigationItem.title =title;
}

+ (void)styleNavigationItem:(UINavigationItem *)navigationItem WithTitle:(NSString *)title
{
    navigationItem.title =title;
    navigationItem.hidesBackButton = YES;
}

@end
