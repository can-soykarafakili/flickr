//
//  FlickrMainPageViewController.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrMainPageViewController.h"
#import "FlickrViewControllerBuilder.h"
#import "FlickrNavigationBarStyler.h"
#import <Masonry/Masonry.h>
#import <CoreLocation/CoreLocation.h>
#import "NSString+SearchText.h"

static dispatch_once_t once;

@interface FlickrMainPageViewController () <UISearchBarDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>

@property (nonatomic,strong) UIButton *partyButton;
@property (nonatomic,strong) UILabel *partyLabel;
@property (nonatomic,strong) UIButton *nearMeButton;
@property (nonatomic,strong) UILabel *nearMeLabel;
@property (nonatomic,strong) UIButton *trendingButton;
@property (nonatomic,strong) UILabel *trendingLabel;
@property (nonatomic,strong) UISearchBar *searchBar;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *latitude;

@end

@implementation FlickrMainPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [FlickrNavigationBarStyler styleNavigationItem:self.navigationItem WithTitle:@"Flickr Search"];
    
    [self loadSubviews];
    [self addSubviews];
    [self addLayoutConstraints];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.searchBar.text = nil;
    self.navigationController.navigationBarHidden = NO;
}

- (void)loadSubviews
{
    self.searchBar = [[UISearchBar alloc] init];
    [self.searchBar setTintColor:[UIColor blackColor]];
    self.searchBar.delegate = self;
    [self.searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    self.searchBar.placeholder = @"Search";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    self.partyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.partyButton addTarget:self action:@selector(getPartyPhotosFromFlickr) forControlEvents:UIControlEventTouchUpInside];
    [self.partyButton setBackgroundImage:[UIImage imageNamed:@"icnParty"] forState:UIControlStateNormal];
    
    self.partyLabel = [[UILabel alloc] init];
    self.partyLabel.numberOfLines = 0;
    self.partyLabel.text = @"#Party";
    self.partyLabel.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:12.0];
    self.partyLabel.textAlignment = NSTextAlignmentCenter;
    
    self.nearMeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.nearMeButton addTarget:self action:@selector(getNearbyPhotosFromFlickr) forControlEvents:UIControlEventTouchUpInside];
    [self.nearMeButton setBackgroundImage:[UIImage imageNamed:@"icnNearme"] forState:UIControlStateNormal];
    
    self.nearMeLabel = [[UILabel alloc] init];
    self.nearMeLabel.numberOfLines = 0;
    self.nearMeLabel.text = @"Near Me";
    self.nearMeLabel.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:12.0];
    self.nearMeLabel.textAlignment = NSTextAlignmentCenter;
    
    self.trendingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.trendingButton addTarget:self action:@selector(getInterestingPhotosFromFlickr) forControlEvents:UIControlEventTouchUpInside];
    [self.trendingButton setBackgroundImage:[UIImage imageNamed:@"icnInteresting"] forState:UIControlStateNormal];
    
    self.trendingLabel = [[UILabel alloc] init];
    self.trendingLabel.numberOfLines = 0;
    self.trendingLabel.text = @"Interesting";
    self.trendingLabel.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:12.0];
    self.trendingLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)addSubviews
{
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.partyButton];
    [self.view addSubview:self.partyLabel];
    [self.view addSubview:self.nearMeButton];
    [self.view addSubview:self.nearMeLabel];
    [self.view addSubview:self.trendingButton];
    [self.view addSubview:self.trendingLabel];
}

- (void)addLayoutConstraints
{
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_topLayoutGuide).mas_equalTo(22);
        make.left.equalTo(self.view.mas_left).mas_equalTo(8);
        make.right.equalTo(self.view.mas_right).mas_equalTo(-8);
    }];
    
    [self.partyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 60));
        make.top.equalTo(self.searchBar.mas_bottom).mas_equalTo(22);
        make.left.equalTo(self.searchBar.mas_left).mas_equalTo(40);
        
    }];
    
    [self.partyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 15));
        make.top.equalTo(self.partyButton.mas_bottom).mas_equalTo(12);
        make.centerX.equalTo(self.partyButton);
        
    }];
    
    [self.nearMeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 60));
        make.top.equalTo(self.searchBar.mas_bottom).mas_equalTo(22);
        make.centerX.equalTo(self.view);
        
    }];
    
    [self.nearMeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 15));
        make.top.equalTo(self.nearMeButton.mas_bottom).mas_equalTo(12);
        make.centerX.equalTo(self.nearMeButton);
        
    }];
    
    [self.trendingButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 60));
        make.top.equalTo(self.searchBar.mas_bottom).mas_equalTo(22);
        make.right.equalTo(self.searchBar.mas_right).mas_equalTo(-40);

    }];
    
    [self.trendingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 15));
        make.top.equalTo(self.trendingButton.mas_bottom).mas_equalTo(12);
        make.centerX.equalTo(self.trendingButton);
        
    }];
}

#pragma mark - Actions

- (void)getPartyPhotosFromFlickr
{
    FlickrRequest *requestObject = [FlickrRequest new];
    requestObject.type = @"Tag";
    requestObject.tag = @"Party";
    
    UIViewController * vc = [FlickrViewControllerBuilder resultsViewControllerWithRequestObject:requestObject];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getNearbyPhotosFromFlickr
{
    [self getCurrentLocation];
}

- (void)getInterestingPhotosFromFlickr
{
    FlickrRequest *requestObject = [FlickrRequest new];
    requestObject.type = @"Interesting";
    
    UIViewController * vc = [FlickrViewControllerBuilder resultsViewControllerWithRequestObject:requestObject];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UISearchBar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    FlickrRequest *requestObject = [FlickrRequest new];
    requestObject.type = @"Tag";
    requestObject.tag = [searchBar.text formatText];
    
    UIViewController * vc = [FlickrViewControllerBuilder resultsViewControllerWithRequestObject:requestObject];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) dismissKeyboard
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - CLLocationManager Delegate

- (void)getCurrentLocation
{
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    once = 0;
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Try Again",nil];
    [errorAlert show];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    [manager stopUpdatingLocation];
    manager = nil;
    
    dispatch_once(&once, ^{
        
        CLLocation *currentLocation = newLocation;
        
        if (currentLocation != nil)
        {
            self.longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            self.latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        }
        
        [self.locationManager stopUpdatingLocation];
        
        FlickrRequest *requestObject = [FlickrRequest new];
        requestObject.type = @"Near Me";
        requestObject.latitude = self.latitude;
        requestObject.longitude = self.longitude;
        
        UIViewController * vc = [FlickrViewControllerBuilder resultsViewControllerWithRequestObject:requestObject];
        [self.navigationController pushViewController:vc animated:YES];    });
    
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self.locationManager stopUpdatingLocation];
            break;
        case 1:
            [self.locationManager startUpdatingLocation];
            break;
        default:
            break;
    }
}

@end
