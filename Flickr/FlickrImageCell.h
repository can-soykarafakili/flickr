//
//  FlickrImageCell.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrImageCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *imageView;

+ (NSString *)reuseIdentifier;
+ (CGSize )sizeForWidth:(CGFloat)width;

@end
