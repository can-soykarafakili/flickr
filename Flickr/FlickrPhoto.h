//
//  FlickrPhoto.h
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrPhoto : NSObject

@property (nonatomic,strong) NSURL *photoURL;

@end
