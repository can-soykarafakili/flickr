//
//  UIView+Loading.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "UIView+Loading.h"
#import <MBProgressHUD.h>

static const NSInteger kHudTag = 100;

@implementation UIView (Loading)

- (void)showLoadingView
{
    if (![self viewWithTag:kHudTag])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
        [hud setTag:kHudTag];
        hud.color = [UIColor blackColor];
    }
}

- (void)dismissLoadingView
{
    [MBProgressHUD hideHUDForView:self animated:NO];
}

@end
