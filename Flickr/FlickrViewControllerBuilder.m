//
//  FlickrViewControllerBuilder.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "FlickrViewControllerBuilder.h"
#import "FlickrNavigationController.h"
#import "FlickrMainPageViewController.h"
#import "FlickrResultsViewController.h"
#import "FlickrPhotoDetailViewController.h"

@implementation FlickrViewControllerBuilder

+ (UIViewController *)rootViewController
{
    FlickrNavigationController *navigationController = [[FlickrNavigationController alloc] initWithRootViewController:[self mainPageViewController]];
    return navigationController;
}

+ (UIViewController *)mainPageViewController
{
    FlickrMainPageViewController *vc = [FlickrMainPageViewController new];
    return vc;
}

+ (UIViewController *)resultsViewControllerWithRequestObject:(FlickrRequest *)requestObject
{
    FlickrResultsViewController *vc = [FlickrResultsViewController new];
    vc.requestObject = requestObject;
    return vc;
}

+ (UIViewController *)photoDetailViewControllerWithPhotoObject:(FlickrPhoto *)photo
{
    FlickrPhotoDetailViewController *vc = [FlickrPhotoDetailViewController new];
    vc.photo = photo;
    return vc;
}

@end
