//
//  ScrollViewCentered.m
//  Flickr
//
//  Created by Can SOYKARAFAKILI on 26/03/16.
//  Copyright © 2016 Can SOYKARAFAKILI. All rights reserved.
//

#import "ScrollViewCentered.h"
#import "UIView+Centering.h"

@implementation ScrollViewCentered

//Override layoutSubviews method and add our center method to it.
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self centerScrollViewContent];
}

//Custom method to center the content of this UIScrollView.
- (void)centerScrollViewContent
{
    if(self.delegate && [self.delegate respondsToSelector: @selector(viewForZoomingInScrollView:)])
    {
        UIView *viewForCentering = [self.delegate viewForZoomingInScrollView:self];
        [viewForCentering centerViewInScrollView:self];
    }
    
}

@end
